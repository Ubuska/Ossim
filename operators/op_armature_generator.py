import bpy
import bmesh
import os
from enum import Enum


class TransformAction(Enum):
    Ignore = 1  # type: int
    Freeze = 2  # type: int
    Reset = 3   # type: int


class ArmatureGenerator(bpy.types.Operator):
    """Creates Skeletal Mesh from simulation - armature for every mesh object with root bone at the top of the hierarchy. Also duplicates source mesh objects with no simulation applied but skinned to bones."""
    bl_idname = "object.bake_simulation"
    bl_label = "Generate Armature"

    collection_main_name = 'Ossim'
    collection_skinned_name = 'Skinned Meshes'
    collection_output_name = 'Armature'

    collection_root = None
    collection_main = None
    collection_output = None
    collection_skinned = None

    @classmethod
    def poll(cls, context):
        return bpy.context.view_layer.objects.active is not None

    @classmethod
    def deselect(cls, context):
        bpy.ops.object.select_all(action='DESELECT')
        context.scene.objects.active = None

    @classmethod
    def generate_bone_per_vertex(cls, context, of_object: object):
        print("\n -generating bone per vertex for object %s." % of_object.name)

        armatures = []
        for vertex in of_object.data.vertices:
            vertex_location = of_object.matrix_world @ vertex.co
            bpy.ops.object.armature_add(enter_editmode=True, location=(0, 0, 0))
            bpy.ops.armature.select_all(action='TOGGLE')
            bpy.ops.transform.translate(value=vertex_location)
            bpy.ops.object.mode_set(mode='OBJECT')

            armature_master = context.view_layer.objects.active
            armature_master.parent = of_object
            armature_master.parent_type = 'VERTEX'

            # Only pass on the index, not the vertex object.
            armature_master.parent_vertices[0] = vertex.index
            armatures.append(armature_master)

        print("- created %s individual bones." % len(armatures))
        return armatures

    @classmethod
    def generate_union_armature(cls, context, armatures: [], name: str, collection_container):
        print("\n generating union armature with constraints for %s bones." % len(armatures))
        created_armatures = []
        for index, arm in enumerate(armatures):
            arm.select_set(state=False)
            world_translation = arm.matrix_world.to_translation()
            bpy.ops.object.armature_add(enter_editmode=True, location=world_translation)
            bpy.ops.armature.select_all(action='TOGGLE')
            bpy.ops.transform.translate(value=world_translation)
            bpy.ops.object.mode_set(mode='OBJECT')
            armature_slave = context.view_layer.objects.active
            created_armatures.append(armature_slave)

            index_prefix = ""
            if index + 1 < 10: index_prefix = "00"
            if 100 > index + 1 >= 10: index_prefix = "0"
            index_string = index_prefix + str(index + 1)
            arm.name = "{name}_{index}".format(name = name, index = index_string)

            print("- new bone %s with world translation: %s" % (armature_slave.name, world_translation))
            bpy.ops.object.posemode_toggle()
            bpy.ops.pose.constraint_add(type='COPY_LOCATION')
            context.view_layer.objects.active.pose.bones[0].constraints["Copy Location"].target = arm
            bpy.ops.object.posemode_toggle()

            collection_container.objects.link(arm)  # add to scene
            arm.hide_viewport = True

        print("Created armature with %s bones. \n" % len(created_armatures))

        for arm in created_armatures:
            arm.select_set(state=True)
            bpy.ops.object.join()
        merged_arms = bpy.context.view_layer.objects.active

        return merged_arms

    @classmethod
    def bind_geometry(cls, bind_object : object, armature : object):
        print("- binding %s to armature %s." % (bind_object.name, armature.name))
        bpy.context.view_layer.objects.active = bind_object
        bpy.context.view_layer.objects.active = armature
        bind_object.select_set(state=True)
        armature.select_set(state=True)
        bpy.context.view_layer.objects.active = armature
        bpy.ops.object.parent_set(type='ARMATURE_AUTO')
        if bpy.context.scene.parent_type == 'OBJECT':       bpy.ops.object.parent_type = 'OBJECT'
        elif bpy.context.scene.parent_type == 'ARMATURE':   bpy.ops.object.parent_type = 'ARMATURE'

    @classmethod
    def reduce_geometry(cls, context, reduce_target_object):
        print("- reducing geometry for %s." % reduce_target_object.name)
        context.view_layer.objects.active = reduce_target_object

        # This is an Object with a Mesh, see if it has the supported group name.
        found = False
        reduce_group_name = bpy.context.scene.vgr
        for i in range(0, len(reduce_target_object.vertex_groups)):
            group = reduce_target_object.vertex_groups[i]
            if group.name == reduce_group_name:
                found = True

        # Selecting group (or all) vertices.
        context.view_layer.objects.active.editmode_toggle()
        bpy.ops.object.mode_set(mode='EDIT')
        return
        if found:
            cls.select_vertices(context.active_object, reduce_group_name)
            bpy.ops.mesh.select_all(action='INVERT')
        else:
            bpy.ops.mesh.select_all()

        bpy.ops.mesh.unsubdivide(iterations=context.scene.bone_vertex_frequency_decrease)
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode='OBJECT')

    @classmethod
    def clean_object(cls, clean_object):
        if not clean_object.modifiers:
            return
        clean_object.modifiers.remove(clean_object.modifiers.get("Cloth"))

    @classmethod
    def duplicate_object(cls, source_object: object, linked: object = False, action: object = TransformAction.Ignore) -> object:
        print("- duplicating object %s." % source_object.name)

        duplicated_object = source_object.copy()  # duplicate linked
        if not linked: duplicated_object.data = source_object.data.copy()  # optional: make this a real duplicate (not linked)

        # @Todo, @Incomplete: Reintroduce this using 2.8 API
        #context.scene.objects.link(skin_object)  # add to scene
        #    context.view_layer.objects.link(skin_object)

        cls.clean_object(duplicated_object)
        if action == TransformAction.Reset:
            print("- duplicate transform action: reset.")
            duplicated_object.location = (0, 0, 0)
            duplicated_object.rotation_euler = (0, 0, 0)
            duplicated_object.scale[0] = 1
            duplicated_object.scale[1] = 1
            duplicated_object.scale[2] = 1
        elif action == TransformAction.Freeze:
            print("- duplicate transform action: freeze.")

        return duplicated_object

    @classmethod
    def select_vertices(cls, of_object, group_name):
        bpy.ops.object.mode_set(mode='EDIT')
        mesh = of_object.data
        bm = bmesh.from_edit_mesh(mesh)

        # This is an Object with a Mesh, see if it has the supported group name.
        group_index = -1
        for i in range(0, len(of_object.vertex_groups)):
            group = of_object.vertex_groups[i]
            if group.name == group_name:
                group_index = i

        print("- checking %s for assigned vertices." % of_object.name)
        indices = []

        # Now access the vertices that are assigned to this group
        for v in mesh.vertices:
            for vertGroup in v.groups:
                if vertGroup.group == group_index:
                    indices.append(v.index)
                    #print("Vertex %d is part of group." % v.index)

        vertices = [e for e in bm.verts]

        for vert in vertices:
            if vert.index in indices:   vert.select = True
            else:                       vert.select = False

        bmesh.update_edit_mesh(mesh, True)
        bpy.ops.object.mode_set(mode='EDIT')

    def generate_cloth(self, context):
        is_optimization_active = context.scene.bone_vertex_frequency_decrease > 1
        cloth_geometry_object = bpy.context.view_layer.objects.active

        # Generate bone per vertex.
        print ("- generating union armature")
        armatures = self.generate_bone_per_vertex(context, cloth_geometry_object)

        # Generate union armature A with constraints.
        merged_armature = self.generate_union_armature(context, armatures, cloth_geometry_object.name, self.collection_skinned)
        merged_armature.name = "{name}_{geometry_name}".format(name = context.scene.armature_name, geometry_name = cloth_geometry_object.name)

        # Duplicate geometry to skin it, bind to armature.
        duplicated_cloth_geometry = self.duplicate_object(cloth_geometry_object, False, TransformAction.Freeze)  # type: object
        duplicated_cloth_geometry.name = context.scene.skin_name

        # Move to Skin collection.
        self.exclusive_collection_set(duplicated_cloth_geometry, self.collection_skinned)
        self.bind_geometry(duplicated_cloth_geometry, merged_armature)
        cloth_geometry_object.name = context.scene.source_name

        # Parent bones to root.
        print("- parent bones to the root")
        bpy.ops.object.editmode_toggle()
        for bone in context.visible_bones: bone.parent = context.object.data.edit_bones[0]
        bpy.ops.object.editmode_toggle()

        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = duplicated_cloth_geometry
        duplicated_cloth_geometry.select_set(state=True)

    def does_have_vertex_group(self, object, group_name):
        for i in range(0, len(object.vertex_groups)):
            group = object.vertex_groups[i]
            if group.name == group_name:
                return True

    @staticmethod
    def find_collection(collection_name: str):
        if bpy.data.collections.get(collection_name) is None:
            return bpy.data.collections.new(collection_name)
        return bpy.data.collections.get(collection_name)
        
    def generate_rigidbody_bone(self, context, index: int, object):
        index_string = str(index + 1)
        if index < 10: index_string = "0" + index_string
            
        bone_name_string = context.scene.bone_name
        if len(bone_name_string) < 1: bone_name_string = "bone"
        bone_name = "{name}_{idx}".format(name = bone_name_string, idx = index_string)
        skin_name = "{name}_{idx}".format(name = context.scene.skin_name, idx = index_string)
        armature_name = "{name}".format(name = context.scene.armature_name)
        print("- [{index}]: processing object - {name}".format(name = object.name, index = index_string))

        # Duplicate geometry to skin it.
        skin_object = object.copy()  # duplicate linked

        if not context.scene.link_object:
            skin_object.data = object.data.copy()  # optional: make this a real duplicate (not linked)
        self.exclusive_collection_set(skin_object, self.collection_skinned)

        skin_object.location = (0, 0, 0)
        skin_object.rotation_euler = (0, 0, 0)
        skin_object.scale = (1, 1, 1)
        skin_object.select_set(state=True)
        context.view_layer.objects.active = skin_object

        if skin_object.rigid_body:      bpy.ops.rigidbody.object_remove()
        if skin_object.animation_data:  context.object.animation_data_clear()
        skin_object.name = skin_name

        print("   -- duplicated source_mesh, new duplicated object name: " + skin_object.name)
        context.view_layer.objects.active = object

        # Creating armature.
        armature = bpy.ops.object.armature_add(enter_editmode=False, location=(0, 0, 0))
        armature_object = context.active_object
        self.exclusive_collection_set(armature_object, self.collection_output)

        # Assigning copy transform to new armature bone.
        bpy.ops.object.posemode_toggle()  # Enter pose mode.
        constraint = bpy.ops.pose.constraint_add(type='COPY_TRANSFORMS')
        armature_object.pose.bones[0].constraints[0].target = object
        armature_object.pose.bones[0].name = bone_name
        bpy.ops.object.posemode_toggle()  # Leave pose mode.

        armature_object.name = armature_name
        print("   -- created armature named: " + armature_object.name + " with bone copying: " + object.name)

        # Skin duplicated geo to bone.
        skin_object.select_set(state=True)
        context.view_layer.objects.active = skin_object
        if context.scene.clear_location:    bpy.ops.object.location_clear(clear_delta=False)
        if context.scene.clear_rotation:    bpy.ops.object.rotation_clear(clear_delta=False)
        if context.scene.clear_scale:       bpy.ops.object.scale_clear(clear_delta=False)

        armature_object.select_set(state=True)
        context.view_layer.objects.active = armature_object
        bpy.ops.object.parent_set(type='ARMATURE_NAME')
        vertex_group = skin_object.vertex_groups[bone_name]

        # Set parent (armature) type to ARMATURE instead of the default.
        # OBJECT type. Must be invoked after parenting to armature!
        context.view_layer.objects.active = skin_object
        if context.scene.parent_type == 'OBJECT':       skin_object.parent_type = 'OBJECT'
        elif context.scene.parent_type == 'ARMATURE':   skin_object.parent_type = 'ARMATURE'

        # Remove constraints if there are any.
        bpy.ops.object.constraints_clear()

        context.view_layer.objects.active = object
        mesh = skin_object.data
        for vert in mesh.vertices:
            vertex_group.add([vert.index], 1.0, "ADD")
        return skin_object, armature_object

    def exclusive_collection_set(self, target_object, target_collection):
    
        # @Todo: Add a check for collection? If it does not exist, create it.
        # Remove this object from any collection it might be in.
        for collection in bpy.data.collections:
            if collection != target_collection:
                if target_object in collection.objects[:]:
                    collection.objects.unlink(target_object)
        if target_collection.objects.get(target_object.name) is None:
            target_collection.objects.link(target_object)

    def generate_rigidbody_fast(self, context):
        # Create a new armature and enter edit mode
        armature = bpy.data.armatures.new("Armature")
        obj = bpy.data.objects.new("Armature", armature)
        
        bpy.context.collection.objects.link(obj)
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')
        
        from mathutils import Vector
        
        # Create a new vector and add it to obj_iter.location
        offset = Vector((0.0, 0.0, 1.0))
        
        # Iterate over the selected objects
        for obj_iter in bpy.context.selected_objects:
            
            # Create a new bone at the same location as the object
            bone = armature.edit_bones.new(obj_iter.name)
            bone.head = obj_iter.location
            bone.tail = obj_iter.location + offset
        
        # Exit edit mode and enter pose mode
        bpy.ops.object.mode_set(mode='POSE')
        
        # Set the obj variable to the armature object
        obj = bpy.context.active_object
        
        # Iterate over the bones in the armature
        for bone in obj.pose.bones:
            # Add a "Child Of" constraint to the bone and set the target to be the object
            constraint = bone.constraints.new("CHILD_OF")
            constraint.target = bpy.data.objects[bone.name]

    def generate_rigidbody(self, context):
        armatures = []
        skinned_objects = []
        root = None
        root_name = context.scene.root_name

        # Firstly, filter mesh objects.
        print("- filtering selected objects.\n")
        for obj in context.selected_objects:
            obj_type = getattr(obj, 'type', '')
            if obj_type != 'MESH':
                obj.select_set(state=False)

        for index, obj in enumerate(context.selected_objects):
            skin_object, armature_object = self.generate_rigidbody_bone(context, index, obj)
            skinned_objects.append(skin_object)
            armatures.append(armature_object)

        bones_number = len(armatures)
        print("\n- join created armatures ({num}) into a single armature with multiple bones.".format(num = bones_number))
        if len(armatures) > 1:
            for arm in armatures:
                arm.select_set(state=True)
                context.view_layer.objects.active = arm
            bpy.ops.object.join()
        merged_arms = context.active_object
        merged_bones_number = len(merged_arms.data.bones)

        # Create root bone:
        print("- creating root bone: {name}.".format(name = root_name))
        armature = bpy.ops.object.armature_add(enter_editmode=False, location=(0, 0, 0))
        root_object = context.active_object
        root_object.name = context.scene.armature_name
        root_object.data.name = root_object.name
        self.exclusive_collection_set(root_object, self.collection_output)
                    
        bpy.ops.object.posemode_toggle()
        root_object.pose.bones[0].name = root_name
        bpy.ops.object.posemode_toggle()

        # Deselect all bones.
        bpy.ops.object.editmode_toggle()
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.editmode_toggle()

        # Merge armatures and root bone.
        print("- merging bones ({number}) and root bone.".format(number = merged_bones_number))
        merged_arms.select_set(state=True)
        root_object.select_set(state=True)
        context.view_layer.objects.active = root_object
        bpy.ops.object.join()

        # Parent bones to root.
        print("- parent {num} bones to root [{name}].".format(num = merged_bones_number, name = root_name))
        bpy.ops.object.editmode_toggle()
        for bone in context.visible_bones:
            bone.parent = context.active_object.data.edit_bones[0]
        bpy.ops.object.editmode_toggle()

        for skin in skinned_objects:
            skin.modifiers["Armature"].object = root_object
        print("Generation finished.")
        print("-----------------------------------")

    def execute(self, context):
        os.system("cls")
        print("\n-----------------------------------")
        print("Bake simulation to armature started.\n")
        
        self.collection_root = bpy.context.scene.collection
        self.collection_main = self.find_collection(self.collection_main_name)
        self.collection_output = self.find_collection(self.collection_output_name)
        self.collection_skinned = self.find_collection(self.collection_skinned_name)
        
        self.collection_skinned.hide_render = False
        self.collection_skinned.hide_select = False
        self.collection_skinned.hide_viewport = False
        
        if bpy.context.scene.collection.children.get(self.collection_main_name) is None:
            bpy.context.scene.collection.children.link(self.collection_main)
        if self.collection_main.children.get(self.collection_skinned_name) is None:
            self.collection_main.children.link(self.collection_skinned)
        if self.collection_main.children.get(self.collection_output_name) is None:
            self.collection_main.children.link(self.collection_output)
            
        if context.scene.object_type == 'Rigidbody':        self.generate_rigidbody(context)
        if context.scene.object_type == 'Rigidbody_Fast':   self.generate_rigidbody_fast(context)
        elif context.scene.object_type == 'Cloth':          self.generate_cloth(context)
        return {'FINISHED'}


def register():
    bpy.utils.register_class(ArmatureGenerator)


def unregister():
    bpy.utils.unregister_class(ArmatureGenerator)
