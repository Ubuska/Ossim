import sys
import importlib

bl_info = {
    "name": "Ossim",
    "description": "Bakes rigidbody and cloth simulations to armature.",
    "author": "Peter Gubin  http://thewarpgames.com/petergubin",
    "version": (1, 5, 0),
    "blender": (4, 00, 0),
    "location": "3D View > Tools panel",
    "wiki_url": "https://blendermarket.com/products/ossim",
    "category": "3D View"}

modulesNames = ['panels.panel_generator', 'operators.op_armature_generator', 'operators.op_autokeyframe']

if 'DEBUG_MODE' in sys.argv:
    from operators import *
    from panels import *
else:
    from .operators import *
    from .panels import *

modulesFullNames = {}
for currentModuleName in modulesNames:
    if 'DEBUG_MODE' in sys.argv:
        modulesFullNames[currentModuleName] = ('{}'.format(currentModuleName))
    else:
        modulesFullNames[currentModuleName] = ('{}.{}'.format(__name__, currentModuleName))

for currentModuleFullName in modulesFullNames.values():
    if currentModuleFullName in sys.modules:
        importlib.reload(sys.modules[currentModuleFullName])
    else:
        globals()[currentModuleFullName] = importlib.import_module(currentModuleFullName)
        setattr(globals()[currentModuleFullName], 'modulesNames', modulesFullNames)

def register():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            print(currentModuleName)
            if hasattr(sys.modules[currentModuleName], 'register'):
                sys.modules[currentModuleName].register()

def unregister():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'unregister'):
                sys.modules[currentModuleName].unregister()
                
if __name__ == "__main__":
    register()