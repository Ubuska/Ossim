![alt text](https://d1231c29xbpffx.cloudfront.net/store/product/152674/image/largef-6a695c686568bd877736202990d43839.jpg?Expires=1593788728&Signature=CX8ooQw1SSAQYhs~GFEKC55xG9kNhDicRF0UTDHS3N5x2GieKfpH0FzXBbGO0f4kPaOGl4uIQiE9Q7u9rQozihRZv6YH3lesLqUtxjbw49CJ~hdu7qbC~QRAbl0sFCIoO1~p-TqitqLwPuVEf7BM-~eDeND4DN8pWkVlmXH8RDpgz9CjQ1uzfQ6AF8UB4FWz~-h0EUWdWwqa14th8DskBjM4uvS~yd0D7ZNd8Ji4DD-9udY64CVgj65kWK3BAJaPPkXlxVTCrP~iu9efQxm-xPV9kGj2plBDrrsjQbi8~4RI0koktxSgiZabm6spvziKTLb-UXyY9sk8CkyRTwwptg__&Key-Pair-Id=APKAIN6COYBF3ZQW7OQQ)


You can support this project by contributing or by purchasing this addon here:
[Blendermarket](https://blendermarket.com/products/ossim)
[Gumroad](https://gumroad.com/l/ossim)


Ossim is a simulation baking tool. It allows you to generate an armature and skinned geometry based on physics simulation to use 
in game engines and realtime applications such as Unreal Engine 4 or Unity3d and many others.

